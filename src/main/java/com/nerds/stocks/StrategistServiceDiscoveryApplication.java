package com.nerds.stocks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class StrategistServiceDiscoveryApplication {

	public static void main(String[] args) {
		SpringApplication.run(StrategistServiceDiscoveryApplication.class, args);
	}

}
